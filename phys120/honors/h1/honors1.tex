\documentclass[a4paper, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[english]{babel}
\usepackage{amsmath, amssymb}
\usepackage{mdframed}
\usepackage{lipsum}
\usepackage{amsthm}
\usepackage{float}
\usepackage[margin=1in]{geometry}

% figure support
\usepackage{import}
\usepackage{xifthen}
\pdfminorversion=7
\usepackage{pdfpages}
\usepackage{transparent}
\newcommand{\incfig}[1]{%
	\def\svgwidth{\columnwidth}
	\import{./figures/}{#1.pdf_tex}
}

\pdfsuppresswarningpagegroup=1
\theoremstyle{definition}
\newmdtheoremenv{problem}{Problem}
\newtheorem*{definition}{Definition}
\setlength{\parindent}{0.5in}
\setlength{\jot}{20pt}
\AtBeginEnvironment{gather}{\setcounter{equation}{0}}

\begin{document}

\begin{flushright}
	Adam Stewart

	Physics 120

	Dr. McGovern

	\today
\end{flushright}

\begin{center}
	{\Large\textbf{Honors Problem Set 1}\par}
\end{center}

\begin{problem}
Three charges are placed as shown in the figure to the right. The
magnitude of q1 is 2.00 $\mu$C but whether it is positive or negative is
unknown. The value of the charge (sign and magnitude) q2 is unknown.
Charge q3 is +4.00 $\mu$C, and the net force on q3 is entirely to the left.
Considering the different possible signs of q1 and q2 there are four free
body diagrams representing the forces F1 on 3 and F2 on 3 that q1 and q2 exert on q3.
\begin{itemize}
	\item[(a)] Sketch these four free body diagrams
	\item[(b)] Using the sketches from part (a) and the direction of F, 
		deduce the signs of the charges q1 and q2.
	\item[(c)] Calculate the magnitude of q2.
	\item[(d)] Determine F, the magnitude of the net force on q3.
\end{itemize}
\end{problem}
\section*{Part (a)}
\begin{figure}[H]
	\centering
	\incfig{1a}
\end{figure}
\section*{Part (b)} 

\indent 

$\text{q}_{1}$ must be negative and $\text{q}_{2}$ must be positive, because the y part of the forces must 
cancel each other out and that makes the net force point straight left. \par

\section*{Part (c)}
\indent

To find what the charge of q$_{2}$ is we must take the y component of both of the forces and have them equal to 0.
This is because the net force is entirely on the x-axis, and then that leads to the y components being equal
but opposite of each other. Then we can use Coulomb's law to find out the unknown charge.
\begin{figure}[H]
	\centering
	\incfig{1b}
\end{figure}

{\large
\begin{gather}
\vec{\text{F}}_{\text{y}}
=
-\vec{\text{F}}_{1\text{on}3}\sin(\theta)
+
\vec{\text{F}}_{2\text{on}3}\sin(\phi)
=0 
\\
\vec{\text{F}}_{1\text{on}3}\sin(\theta)
=
\vec{\text{F}}_{2\text{on}3}\sin(\phi)
\\
\frac{k\cdot \text{q}_{2}\cdot 4\times 10^{-6}}{0.03^{2}}\cdot \frac{4}{5}
=
\frac{k\cdot 2\times10^{-6}\cdot 4\times 10^{-6}}{0.04^{2}}\cdot \frac{3}{5}
\\
\text{q}_{2}
=
\frac{2\times10^{-6}\cdot 3\cdot 0.03^{2}}{4\cdot 0.004^{2}}
\\
\text{q}_{2}=8.4375\times 10^{-7}\text{C}
\end{gather}
}
\newpage
\section*{Part (d)}
\indent

Now to find the magnitude of the net force we add 
both of the x components together using Coulomb's law 
and the result of the unknown charge q$_{2}$.
{\large
\begin{gather} 
\vec{\text{F}}_{\text{net}}
=
-\vec{\text{F}}_{1\text{on}3}\cos(\theta)
-
\vec{\text{F}}_{2\text{on}3}\cos(\phi)
\\
\vec{\text{F}}_{\text{net}}
=
-\frac{k\cdot 8.4375\times 10^{-7}\cdot 4\times 10^{-6}}{0.03^{2}}\cdot \frac{3}{5}
-
\frac{k\cdot 2\times 10^{-6}\cdot 4\times 10^{-6}}{0.04^{2}}\cdot \frac{4}{5}
\\
\vec{\text{F}}_{\text{net}}
=
-56.2\text{ N}
\end{gather}
}
\newpage

\begin{problem}
Two thin rods of length L lie along the x-axis, one between x = ½a and x = ½a + L and the other between x = -
½a and x = -½a - L. Each rod has positive charge Q distributed uniformly along its length.	
\begin{itemize}
	\item[(a)]	Calculate the electric field produced by the second rod at points along the positive x-axis. 
	\item[(b)] Calculate magnitude of the force that one rod exerts on the other.
\end{itemize}
\end{problem}

\section*{Part (a)}
\indent

To calculate what the electric field produced by a rod 
that has the length of L and is $\frac{1}{2}$a away from the center
of the y axis we must take the summation of all the charges 
along the rod and let the distance be a part of the rod to the y axis plus
the distance from the y axis to some point on the positive x axis, which will
be labeled r.
\begin{figure}[H]
	\centering
	\incfig{2a}
\end{figure}

{\large
\begin{gather}
E=\int_{b}^{a} \frac{kdq}{r^{2}}\hat{r}
\\
dq=\lambda\cdot dx
\\
r=x+r
\\
-\frac{1}{2}\text{a}-\text{L}<x<-\frac{1}{2}\text{a}
\\
E=\int_{-\frac{1}{2}\text{a}-\text{L}}^{-\frac{1}{2}\text{a}}
\frac{k\cdot \lambda dx}{(x+r)^{2}}
\\
E=k\cdot \lambda (\frac{1}{\frac{1}{2}\text{a}+r}
-
\frac{1}{\frac{1}{2}\text{a}+\text{L}+r})
\end{gather}
}

\section*{Part (b)}
\indent

Now to find the total force of a rod along another rod that has 
the same length we must take the summation of the electric field
produced by the left rod multiplied by every charge along the right
rod. 
{\large
\begin{gather}
\text{F} = \int_{b}^{a} Edq 
\\
dq=\lambda dx
\\
r=x
\\
\frac{1}{2}\text{a}<x<\frac{1}{2}\text{a}+\text{L}
\\
Edq=
k\cdot \lambda^{2} (\frac{1}{\frac{1}{2}\text{a}+r}
-
\frac{1}{\frac{1}{2}\text{a}+\text{L}+r})dx
\\
\text{F} = 
k\cdot \lambda^{2} (\int_{\frac{1}{2}\text{a}}^{\frac{1}{2}\text{a}+\text{L}} 
\frac{dx}{\frac{1}{2}\text{a}+x}
-
\int_{\frac{1}{2}\text{a}}^{\frac{1}{2}\text{a}+\text{L}} 
\frac{dx}{\frac{1}{2}\text{a}+\text{L}+x}
)
\\
F=k\cdot \lambda^{2}(
\ln|a+L|-\ln|a|-\ln|a+2L|+\ln|a+l|)
\\
F=k\cdot \lambda^{2}
\ln|\frac{(a+L)^{2}}{a(a+2L)}|
\end{gather}
}
\newpage


\begin{problem}
An infinitely long cylinder of radius R has a linear charge density $\lambda$. The volume charge density (C/m3) within
the cylinder is not constant but given by $\rho$(r) = r$\rho_{0}$/R, where $\rho_{0}$ is a constant to be determined. 	
\begin{itemize}
	\item[(a)] The charge within a small volume dV is dq = $\rho$dV. The integral of $\rho$dV over a cylinder of length L is
the total charge Q = $\lambda$L within the cylinder. Use this fact to show that $\rho_{0}$ = $\frac{3\lambda}{2 \pi R^{2}}$. 
	\item[(b)] Use Gauss's law to find an expression for the electric field inside the cylinder.
\end{itemize}
\end{problem}

\section*{Part a}
\indent

To find the total charge of Q we must take the fact that dq=$\rho$dV, and 
integrate both sides of the equation from 0 to R. Then substitute dV for the 
derivative of the volume of a cylinder with respect to r,
and substitute $\rho$ with its equation. Finally solve for $\rho_{0}$.
{\large
\begin{gather}
	dq=\rho dV
	\\
	\int_{0}^{R} dq 
	=
	\int_{0}^{R} \rho dV 
	\\
	V=\pi\cdot r^{2}\cdot L
	\\
	dV=2\cdot \pi\cdot r\cdot Ldr
	\\
	Q=\int_{0}^{R} \rho \cdot 2\cdot \pi\cdot Ldr
	\\
	Q=\int_{0}^{R} \frac{2\cdot \rho_{0} \cdot r^{2}\cdot \pi\cdot Ldr}{R}
	\\
	\lambda \cdot L=\frac{\rho_{0}\cdot 2\pi\cdot L\cdot R^{2}}{3}
	\\
	\rho_{0}=\frac{3\lambda}{2\pi\cdot R^{2}}
\end{gather}
}

\section*{Part b}
\indent

To find out an expression for the electric field inside of the cylinder
you must find out Q$_{\text{in}}$ using the expression for $\rho$.
Then you use Gauss's law to solve for the electric field 
along the wall of the cylinder.

\begin{figure}[H]
	\centering
	\incfig{3b}
\end{figure}

{\large
\begin{gather}
	Q_{in}=\rho \cdot V
	\\
	Q_{in}=\frac{3\lambda\cdot r^{3}\cdot L}{2\pi\cdot R^{3}}
	\\
	\int E\cdot dA=\frac{Q_{in}}{\epsilon_{0}}
	\\
	E=\frac{3\lambda\cdot r^{2}}{4\pi\cdot R^{3}\cdot \epsilon_{0}}
\end{gather}
}
\newpage

\begin{problem}
A sphere of radius R has total charge Q. The volume charge density (C/m3) within the sphere is $\rho$(r) = C/r$^{2}$,
where C is a constant to be determined.
\begin{itemize}
	\item[(a)]The charge within a small volume dV is dq = $\rho$dV. The integral of $\rho$dV over the entire volume of the
sphere is the total charge Q. Use this fact to determine the constant C in terms of Q and R. 
	\item[(b)] Use Gauss's law to find an expression for the electric field E inside the sphere.
\end{itemize}
\end{problem}

\section*{Part a}
\indent

To find the constant C you must integrate dq and $\rho$dV from 0 to R.
Substitute dV with the derivative of the volume of a sphere with respect to r.
Then substitute $\rho$ with its equation.
Finally solve for C.

{\large
\begin{gather}
Q=\int_{0}^{R} \rho dV
\\
dV=4\pi r^{2}dr
\\
\rho = \frac{C}{r^{2}}
\\
Q = \int_{0}^{R} 4\pi \cdot Cdr
\\
C=\frac{Q}{4\pi\cdot R}
\end{gather}
}

\newpage
\section*{Part b}
\indent

First we substitute Q $_{\text{in}}$ for the expression of $\rho$ multiplied by the 
volume of a sphere. Then we take Gauss's law and find solve for the electric field.

\begin{figure}[H]
	\centering
	\incfig{4b}
\end{figure}
{\large
\begin{gather}
	Q_{\text{in}}=\rho\cdot V
	\\
	Q_{\text{in}}=\frac{Q\cdot r}{3\cdot R}
	\\
	\int EdA = \frac{Q_{\text{in}}}{\epsilon_{0}}
	\\
	E\cdot 4\pi\cdot r^{2}=\frac{Q\cdot r}{3\cdot R\cdot \epsilon_{0}}
	\\
	E=\frac{Q}{12\pi\cdot R\cdot r\cdot \epsilon_{0}}
\end{gather}
}
\end{document}
